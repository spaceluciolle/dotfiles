# Colors
background='#000000'
foreground='#d7d6d6'
cursorColor='#d7d6d6'

color0='#181818'
color1='#fc1810'
color2='#01a754'
color3='#e68523'
color4='#5b7dba'
color5='#9a2d2d'
color6='#3e557e'
color7='#bfbebe'
color8='#616060'
color9='#a10f0a'
color10='#01773c'
color11='#fbbc09'
color12='#1770c1'
color13='#ed455e'
color14='#125796'
color15='#d7d6d6'

# FZF colors
export FZF_DEFAULT_OPTS="
    $FZF_DEFAULT_OPTS
    --color fg:-1,bg:-1,hl:1,fg+:232,bg+:1,hl+:255
    --color info:7,prompt:2,spinner:1,pointer:232,marker:1
"
