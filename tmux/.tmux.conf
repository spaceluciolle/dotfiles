set -g status on
set -g pane-base-index 1
set -g base-index 1
set -g set-titles on
set -g default-terminal "screen-256color"
set -sg escape-time 10

# prefix (caps + a)
set -g prefix C-space
# reload config
bind R source-file "~/.tmux.conf"
# enable mouse
set -g mouse on

# vim like copy
setw -g mode-keys vi
set -g status-keys emacs

bind-key -T copy-mode-vi 'v' send -X begin-selection
bind-key -T copy-mode-vi 'V' send -X select-line
bind-key -T copy-mode-vi 'r' send -X rectangle-toggle
bind-key -T copy-mode-vi 'y' send -X copy-pipe-and-cancel "xclip -in -selection clipboard"

# move between pane
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R
bind -n M-h select-pane -L
bind -n M-j select-pane -D 
bind -n M-k select-pane -U
bind -n M-l select-pane -R

# create new tab and windows in the same path
bind '"' split-window -c "#{pane_current_path}"
bind % split-window -h -c "#{pane_current_path}"
bind c new-window -c "#{pane_current_path}"
# split with more intuitive (v : vs / s : split)
bind-key v split-window -h -c "#{pane_current_path}"
bind-key s split-window -v -c "#{pane_current_path}"

# switch windows n p
bind -n M-n select-window -n
bind -n M-p select-window -p

set-option -g set-titles-string '#{pane_current_command}'
set-option -g history-limit 1000
set-option -g visual-activity on
set-option -g status-position bottom
set-window-option -g monitor-activity on

# Split
set-option -g pane-active-border-style fg=green,bg=default
set-option -g pane-border-style fg=colour8,bg=default

# Status
set -g status-left ''
set -g status-right '#[fg=red]#S  #[fg=magenta]∙ #[fg=blue] #T '
set -g status-right-length 100
set -g status-bg default
setw -g window-status-format '#[fg=colour8] #I #[fg=colour8,bold] #W '
setw -g window-status-current-format '#[fg=green] #I #[fg=green,bold] #W '
setw -ga window-status-activity-style fg=red

