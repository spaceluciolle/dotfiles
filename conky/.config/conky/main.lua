require 'cairo'

package.path = package.path .. ';' .. os.getenv('HOME') .. '/.config/conky/?.lua'
require 'cpu'
require 'fan'
require 'locale'
require 'wireless'
require 'battery'
require 'fs'
require 'mem'

function conky_main()
  if conky_window == nil then
    return
  end
  local cs = cairo_xlib_surface_create(conky_window.display,
  conky_window.drawable,
  conky_window.visual,
  conky_window.width,
  conky_window.height)
  local cr = cairo_create(cs)
  local updates=tonumber(conky_parse('${updates}'))

  local font_size = 10
  local line_spacing = 12

  cairo_select_font_face (cr, "Dina", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL)
  cairo_set_font_size (cr, font_size)
  red,green,blue,alpha=1,1,1,1
  cairo_set_line_width (cr,line_width)
  cairo_set_line_cap  (cr, line_cap)
  cairo_set_source_rgba (cr,red,green,blue,alpha)
  line_cap=CAIRO_LINE_CAP_BUTT
  line_width=1


  --applet
  cpu(cr, line_spacing)
  fan(cr, line_spacing)
  layout(cr, line_spacing)
  battery(cr, line_spacing)
  fs(cr, line_spacing)
  wireless(cr, line_spacing)
  mem(cr, line_spacing)

  cairo_destroy(cr)
  cairo_surface_destroy(cs)
  cr=nil
end

function group(cr, start, bottom_right, title)

  -- set title position
  local extents = cairo_text_extents_t:create()
  cairo_text_extents(cr, title, extents)

  local title_begin_x = start.x + (bottom_right.x)/2 - extents.width/2
  local title_end_x = title_begin_x + extents.width
  local title_middle = start.y - extents.height/2

  -- top corner
  cairo_move_to(cr, start.x, title_middle)
  cairo_line_to(cr, title_begin_x, title_middle)
  cairo_move_to(cr, title_end_x, title_middle)
  cairo_line_to(cr, start.x + bottom_right.x, title_middle)

  -- left corner
  cairo_line_to(cr, start.x + bottom_right.x, start.y + bottom_right.y)
  cairo_line_to(cr, start.x, start.y + bottom_right.y)
  cairo_line_to(cr, start.x, title_middle)

  -- text
  cairo_move_to(cr, title_begin_x, start.y)
  cairo_show_text(cr, title)

  cairo_stroke(cr)

end

function table_merge(table1, table2)
  for k,v in pairs(table2) do table.insert(table1, v) end
  return table1
end

function display_lines(cr, lines, start_x, start_y, line_spacing)
  local y_text = 0
  for line in ipairs(lines) do
    y_text = y_text + line_spacing
    cairo_move_to(cr, start_x, start_y + y_text)
    cairo_show_text(cr, ' ' .. lines[line])
  end
  cairo_stroke(cr)
end

