function battery(cr, line_spacing)
  local top = {x = 645, y = 495}
  local bottom = {x = 145, y=40}
  group(cr, top, bottom, "BATTERY")

  status = conky_parse("${battery_status}")
  time = conky_parse("${battery_time}")
  if status == 'unknown' then
    status = 'threshold'
    time = 'N/A'
  end

  local lines = {
    "CHARGE: " .. conky_parse("${battery_percent}" .. "%"),
    "STATUS: " .. string.upper(status),
    "TIME:   " .. time,
  }

  display_lines(cr, lines, top.x, top.y, line_spacing)
end
