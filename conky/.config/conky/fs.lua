function fs(cr, line_spacing)
  local top = {x = 645, y = 620}
  local bottom = {x = 195, y=75}

  group(cr, top, bottom, "FILE SYSTEM")

  local lines = table_merge(fs_lines('/home'), fs_lines('/'))

  display_lines(cr, lines, top.x, top.y, line_spacing)
end

function fs_lines(fs)
  local info = fs_info(fs)
  local fs_text = fs
  if fs == '/' then
    fs_text = "ROOT"
  end
  local lines = {
    string.upper(fs_text):gsub('^/', '') .. ': ',
    '    SPACE: ' .. info.used:gsub(',', '.') .. '/' .. info.size:gsub(',', '.'),
    '    TYPE:  ' .. info.type
  }
  return lines
end

function fs_info(fs)
  local info = {
    used = conky_parse("${fs_used " .. fs .. "}"),
    size = conky_parse("${fs_size " .. fs .. "}"),
    type = conky_parse("${fs_type " .. fs .. "}"),
  }
  return info
end
