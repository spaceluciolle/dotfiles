" visual helper
set nocompatible
syntax on
set number
set rnu
set ruler
set showcmd

"incremental search
set hlsearch
set incsearch
if has('nvim')
  set inccommand=nosplit
endif

" two space auto-indent
set autoindent
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab

set wrap
set linebreak


" auto-complete with <Tab> in command mode
set wildmenu
set wildmode=longest,full
noremap <Space> <Nop>
lmap <Space> <Leader>


" three mappings I can’t live without
nmap Y y$
nmap U <C-r>

set background=dark
colorscheme gruvbox

autocmd BufNewFile,BufRead *.lds set filetype=ld

call plug#begin('~/.vim/plugged')
  Plug 'derekwyatt/vim-scala'
  Plug 'morhetz/gruvbox'
  colorscheme gruvbox
call plug#end()
