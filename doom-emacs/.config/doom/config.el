;;; ~/.doom.d/config.el -*- lexical-binding: t; -*-
;; Place your private configuration here
(require 'org-drill)
(setq org-drill-learn-fraction 0.45)
(require 'iso-transl)
(setq skk-large-jisyo "/usr/share/skk/SKK-JISYO.L")
(setq default-input-method "japanese-skk")
(set-face-font 'default "monospace:pixelsize=13:antialias=false:hinting=false")
(set-fontset-font t 'japanese-jisx0208 (font-spec :name "IPAPGothic" :size 16))
(setq-default ispell-program-name "hunspell")
(setq ispell-dictionary "francais")
(add-hook 'org-mode-hook 'turn-on-flyspell)
(load-theme 'doom-gruvbox t)
(load! "bindings")
(when (executable-find "hunspell")
  (setq-default ispell-program-name "hunspell")
  (setq ispell-really-hunspell t))

(add-to-list 'org-src-lang-modes '("plantuml" . plantuml))
(setq plantuml-jar-path "/path/to/your/copy/of/plantuml.jar")
(setq org-plantuml-jar-path "/usr/share/java/plantuml-1.2020.2.jar")

(setq plantuml-executable-path "/usr/bin/plantuml")
(setq plantuml-default-exec-mode 'executable)

(setq display-line-numbers-type 'relative)
(add-to-list 'org-latex-packages-alist '("" "minted"))
(setq org-latex-listings 'minted)

(setq org-latex-pdf-process
      '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
        "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
        "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))
